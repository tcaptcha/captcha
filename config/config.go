package config

import (
	"time"
)

type AppCfg struct {
	Port            *int64         `yaml:"port"`
	ShutdownTimeout *time.Duration `yaml:"shutdown_timeout"`
	CaptchaDuration *time.Duration `yaml:"captcha_duration"`
	CanvasWidth     *int64         `yaml:"canvas_width"`
	CanvasHeight    *int64         `yaml:"canvas_height"`
}
