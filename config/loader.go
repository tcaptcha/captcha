package config

import (
	"io/ioutil"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

func LoadConfig(logger *logrus.Logger, configFile string) *AppCfg {
	cfg := &AppCfg{}

	yamlBuf, err := ioutil.ReadFile(configFile)
	if err != nil {

		logger.Fatalf("filed read config yaml file: %s", err.Error())
	}

	if err = yaml.Unmarshal(yamlBuf, cfg); err != nil {

		logger.Fatalf("failed yaml unmarshal: %s", err.Error())
	}

	return cfg
}
