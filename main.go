package main

import (
	"sync"

	"gitlab.com/tcaptcha/captcha/config"
	log "gitlab.com/tcaptcha/captcha/logger"
	"gitlab.com/tcaptcha/captcha/server"
)

func main() {
	logger := log.InitLogger(true)
	appCfg := config.LoadConfig(logger, "local.yaml")

	srv := server.NewServer(logger, *appCfg.Port)

	// start health checking
	wg := &sync.WaitGroup{}

	wg.Add(1)
	go srv.ListenAndServe(wg)

	srv.WaitForShutdown(wg, *appCfg.ShutdownTimeout)

}
