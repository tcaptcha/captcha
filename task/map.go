package task

import (
	"sync"
	"time"

	uuid "github.com/satori/go.uuid"
)

type Map struct {
	taskMap map[uuid.UUID]*Task
	//timers     map[uuid.UUID]*time.Timer
	//captchaDuration time.Duration
	mu *sync.Mutex
}

func NewMap() *Map {

	return &Map{
		taskMap: make(map[uuid.UUID]*Task),
		//timers:     make(map[uuid.UUID]*time.Timer),
		//captchaDuration: captchaDuration,
		mu: new(sync.Mutex),
	}
}

func (m *Map) AddTask(s *Task) {
	m.mu.Lock()
	defer m.mu.Unlock()

	m.taskMap[s.GetSessionUuid()] = s
	
	go m.expireTask(s.GetSessionUuid(), s.GetTimer().C)
}

func (m *Map) expireTask(sessionUuid uuid.UUID, tick <-chan time.Time) {
	<-tick

	m.mu.Lock()
	defer m.mu.Unlock()

	if _, ok := m.taskMap[sessionUuid]; ok {
		delete(m.taskMap, sessionUuid)

	}
}

func (m *Map) RemoveTask(sessionUuid uuid.UUID) {
	if task, ok := m.taskMap[sessionUuid]; ok {
		task.ResetTimer()

	}
}
