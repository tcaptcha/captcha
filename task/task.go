package task

import (
	"image"
	"math"
	"math/rand"
	"strings"
	"time"

	"github.com/fogleman/gg"
	"github.com/golang/freetype/truetype"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"golang.org/x/image/font/gofont/goregular"
)

type Task struct {
	sessionUuid uuid.UUID
	createdAt   time.Time
	//pngBase     []byte
	timer      *time.Timer
	wordFirst  string
	wordSecond string
}

func NewTask(dictionary []string, captchaDuration time.Duration) *Task {
	var wordFirst, wordSecond string
	for ind, i := range rand.Perm(len(dictionary)) {
		if ind == 0 {
			wordFirst = dictionary[i]

		} else if ind == 1 {
			wordSecond = dictionary[i]

		} else {

			break
		}
	}

	return &Task{
		sessionUuid: uuid.NewV4(),
		createdAt:   time.Now().UTC(),
		timer:       time.NewTimer(captchaDuration),
		wordFirst:   wordFirst,
		wordSecond:  wordSecond,
	}
}

func (s *Task) GetSessionUuid() uuid.UUID {

	return s.sessionUuid
}

func (s *Task) GetCreatedAt() time.Time {

	return s.createdAt
}

func (s *Task) GetTimer() *time.Timer {

	return s.timer
}

func (s *Task) ResetTimer() {
	_ = s.timer.Reset(time.Nanosecond)

}

func (s *Task) GetSolution() string {
	b := strings.Builder{}

	b.WriteString(s.wordFirst)
	b.WriteRune(' ')
	b.WriteString(s.wordSecond)

	return b.String()
}

func (s *Task) GeneratePngBase(logger *logrus.Logger, canvasWidth, canvasHeight float64) (image.Image, error) {
	font, err := truetype.Parse(goregular.TTF)
	if err != nil {
		logger.Errorf("failed parse font: %v", err)

		return nil, err
	}

	c1, c2 := s.randomColors()

	dcFirst := gg.NewContext(int(canvasWidth/2), int(canvasHeight))
	dcFirst.SetRGB(1, 1, 1)
	dcFirst.Clear()
	dcFirst.SetRGB(c1[0], c1[1], c1[2])
	faceFirst := truetype.NewFace(font, &truetype.Options{
		Size: s.suggestFontSize(len(s.wordFirst), canvasWidth/2, canvasHeight),
	})

	dcFirst.SetFontFace(faceFirst)
	dcFirst.DrawStringAnchored(s.wordFirst, canvasWidth/4, canvasHeight/2, 0.5, 0.5)
	dcFirst.Stroke()
	imageFirst := dcFirst.Image()

	//w, h := dc.MeasureString(s.wordFirst)
	//dcFirst.RotateAbout(gg.Radians(10), canvasWidth/4, canvasHeight/2)
	//dc.DrawRectangle(10, 10, w, h)

	dcSecond := gg.NewContext(int(canvasWidth/2), int(canvasHeight))
	dcSecond.SetRGB(1, 1, 1)
	dcSecond.Clear()
	dcSecond.SetRGB(c2[0], c2[1], c2[2])
	faceSecond := truetype.NewFace(font, &truetype.Options{
		Size: s.suggestFontSize(len(s.wordSecond), canvasWidth/2, canvasHeight),
	})

	dcSecond.SetFontFace(faceSecond)
	//dcSecond.RotateAbout(gg.Radians(10), canvasWidth/4, canvasHeight/2)
	dcSecond.DrawStringAnchored(s.wordSecond, canvasWidth/4, canvasHeight/2, 0.5, 0.5)
	dcSecond.Stroke()
	imageSecond := dcSecond.Image()

	dcSecond.SavePNG("out.png")

	rgba := image.NewRGBA(image.Rect(0, 0, int(canvasWidth), int(canvasHeight)))

	return dcFirst.Image(), nil
}

func (s *Task) suggestFontSize(lenString int, canvasWidth, canvasHeight float64) float64 {

	return math.Max(canvasWidth/float64(lenString), canvasHeight) * 0.8
}

func (s *Task) waveTransform(src image.Image) [][][3]float32 {
	bounds := src.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y
	iaa := make([][][3]float32, height)

	for y := 0; y < height; y++ {
		row := make([][3]float32, width)

		for x := 0; x < width; x++ {
			atX, atY1, atY2 := s.transformXY(x, y, width, height)
			r1, g1, b1, _ := src.At(atX, atY1).RGBA()
			r2, g2, b2, _ := src.At(atX, atY2).RGBA()
			// A color's RGBA method returns values in the range [0, 65535].
			// Shifting by 8 reduces this to the range [0, 255].
			row[x] = [3]float32{
				float32(r1>>8)/2 + float32(r2>>8)/2,
				float32(g1>>8)/2 + float32(g2>>8)/2,
				float32(b1>>8)/2 + float32(b2>>8)/2,
			}
		}

		iaa[y] = row
	}

	return iaa

}

func (s *Task) transformXY(x, y, width, height int) (int, int, int) {
	atY := float64(y/2) + float64(y/2)*math.Sin(float64(x)/float64(width)*math.Pi*3)

	return x, int(math.Floor(atY)), int(math.Ceil(atY))
}

func (s *Task) randomColors() ([3]float64, [3]float64) {
	c1 := [3]float64{}
	c2 := [3]float64{}

	k1 := (0.5 + 0.5*rand.Float64()) * 0.9
	k2 := (0.4 + 0.6*rand.Float64()) * 0.9

	for i := 0; i < 3; i++ {
		c1[i] = k1 * rand.Float64()
		c2[i] = k2 * rand.Float64()
	}

	return c1, c2
}
