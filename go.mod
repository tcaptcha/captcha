module gitlab.com/tcaptcha/captcha

go 1.16

require (
	github.com/fogleman/gg v1.3.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/image v0.0.0-20220617043117-41969df76e82
	gopkg.in/yaml.v2 v2.4.0
)
