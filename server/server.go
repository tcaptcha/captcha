package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

const HandlerPubGetTask = "pub_get_task"               // same for both services
const HandlerPubValidateSolution = "pub_validate_task" // same for both services

type Server struct {
	server      *http.Server
	logger      *logrus.Logger
	solutionMap map[uuid.UUID]string
}

func NewServer(logger *logrus.Logger, port int64) *Server {
	lb := &Server{
		server: nil,
		logger: logger,
	}

	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: http.HandlerFunc(lb.Handler),
	}

	lb.server = server

	return lb
}

// Handler load balances the incoming request
func (s *Server) Handler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == HandlerPubGetTask {

	} else if r.URL.Path == HandlerPubValidateSolution {

	} else {
		http.Error(w, "Bad request", http.StatusBadRequest)
	}
}

func (s *Server) ListenAndServe(wg *sync.WaitGroup) {
	defer wg.Done()

	s.logger.Infof("HTTP server started at: %s", s.server.Addr)

	if err := s.server.ListenAndServe(); err != http.ErrServerClosed {
		s.logger.Errorf("Error during HTTP serving: %s", err)

	} else {
		s.logger.Info("HTTP server is successfully stopped.")

	}
}

func (s *Server) Shutdown(timeout time.Duration) {
	shutdownCtx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	if err := s.server.Shutdown(shutdownCtx); err != nil {
		s.logger.Errorf("HTTP server failed shutdown: %v", err)

	}
}

// WaitForShutdown Serve and gracefully Shutdown
func (s *Server) WaitForShutdown(wg *sync.WaitGroup, timeout time.Duration) {
	defer s.logger.Debugf("captcha service is shutdown.")

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM, syscall.SIGINT)

	wg.Add(1)
	go func() {
		defer wg.Done()
		defer s.Shutdown(timeout)

		<-sigChan
		s.logger.Debugf("captcha service is interrupt.")

	}()

	wg.Wait()
}
