package server

import (
	"bytes"
	"encoding/json"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (s *Server) PubGetCaptcha(w http.ResponseWriter, r *http.Request) {
	solutionLoad := NewTaskDump(sessionUuid, solutionStr)

	if solutionJson, err := json.Marshal(solutionLoad); err != nil {
		l.logger.Errorf("failed marshal json %v", err)

		return false, err
	}

	if taskDump, err := s.getNewTask(); err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)

	} else {
		var buf bytes.Buffer

		if err := s.tpl.Execute(&buf, taskDump); err != nil {
			s.logger.Errorf("failed execute template: %v", err)
			http.Error(w, "Internal error", http.StatusInternalServerError)

		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)

		if _, err := w.Write(buf.Bytes()); err != nil {
			s.logger.Error(err)

		}
	}
}

func (s *Server) PubSendCaptchaSolution(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		s.logger.Debugf("failed parse form: %v", err)
		http.Error(w, "Bad request", http.StatusBadRequest)

	} else if sessionUuid, err := uuid.FromString(r.FormValue(FormSessionUuid)); err != nil {
		s.logger.Debugf("failed parse form: %v", err)
		http.Error(w, "Bad request", http.StatusBadRequest)

	} else if isValid, err := s.checkSolution(sessionUuid, r.FormValue(FormSolution)); err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)

	} else if !isValid {
		http.Error(w, "Captcha solution invalid", http.StatusBadRequest)

	} else {
		s.setCookieAndRedirect(w, r, sessionUuid)

	}
}
