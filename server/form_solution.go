package server

import (
	uuid "github.com/satori/go.uuid"
)

type SolutionLoad struct {
	SessionUuid string `json:"session_uuid"`
	Solution    string `json:"solution"`
}

type SolutionDump struct {
	IsValid bool `json:"is_valid"`
}

func NewSolutionLoad(sessionUuid uuid.UUID, solution string) *SolutionLoad {

	return &SolutionLoad{
		SessionUuid: sessionUuid.String(),
		Solution:    solution,
	}
}
