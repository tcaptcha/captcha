package server

import (
	uuid "github.com/satori/go.uuid"
)

type TaskDump struct {
	SessionUuid string `json:"session_uuid"`
	PngBase     string `json:"png_base"`
}

func NewTaskDump(sessionUuid uuid.UUID, pngBase string) *TaskDump {

	return &TaskDump{
		SessionUuid: sessionUuid.String(),
		PngBase:     pngBase,
	}
}
